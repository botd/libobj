#!/usr/bin/python3 -u
# LOSH - library object shell. no copyright. no LICENSE.
#
# bin/losh

import sys, os ; sys.path.insert(0, os.getcwd())

import logging
import lo
import os
import sys
import time

from lo.csl import Console
from lo.hdl import Event, Handler, dispatch
from lo.shl import get_exception, parse_cli, termsave, termreset

opts = [
    ('-d', '--daemon', 'store_true', False, "enable daemon mode", "daemon"),
    ('-x', '--exec', 'store_true', False, "execute a command", "exec"),
    ('-s', '--shell', 'store_true', False, "enable shell", "shell"),
    ('-w', '--workdir', 'store', str, lo.hd(".losh"), 'set working directory.', 'workdir'),
    ('-m', '--modules', 'store', str, "", 'modules to load.', 'modules'),
    ('-l', '--loglevel', 'store', str, "error", 'set loglevel.', 'level'),
    ('-a', '--logfile', 'store', str, os.path.join(lo.hd(".losh", "logs", "losh.log")), 'file to log to.', 'logfile')
]

def daemon():
    pid = os.fork()
    if pid != 0:
        termreset()
        os._exit(0)
    os.setsid()
    os.umask(0)
    si = open("/dev/null", 'r')
    so = open("/dev/null", 'a+')
    se = open("/dev/null", 'a+')
    os.dup2(si.fileno(), sys.stdin.fileno())
    os.dup2(so.fileno(), sys.stdout.fileno())
    os.dup2(se.fileno(), sys.stderr.fileno())

def execute(main):
    termsave()
    try:
        main()
    except KeyboardInterrupt:
        print("")
    except PermissionError:
        print("you need root permissions.")
    except Exception:
        logging.error(get_exception())
    finally:
        termreset()

c = Console()

def cmds(event):
    event.reply("|".join(c.cmds))    

def main():
    cfg = parse_cli("losh", lo.__version__, opts)
    c.walk("mods")
    c.walk(cfg.modules, True)
    if cfg.exec:
        e = Event()
        e.orig = repr(c)
        e.txt = " ".join(lo.cfg.args)
        dispatch(c, e)
        return
    c.cmds["cmds"] = cmds
    c.start()
    c.wait()
        
execute(main)
os._exit(0)
