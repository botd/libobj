# LOSH - library object shell
#
# cfg.txt

"""
    >>> import lo

    >>> cfg = lo.Cfg()
    >>> cfg.server = "localhost"
    >>> cfg.channel = "#botd"
    >>> cfg.nick = "botddd"
    >>> print(cfg)
    {
        "channel": "#botd",
        "nick": "botddd",
        "server": "localhost"
    }

"""
