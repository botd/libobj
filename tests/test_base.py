# LOSH - library object shell.
#
# test Object and Persist

import lo
import json
import unittest

class Test_Base(unittest.TestCase):

    def test_construct(self):
        o = lo.Object()
        self.assertEqual(type(o), lo.Object)

    def test_cleanpath(self):
        o = lo.Object()
        self.assertEqual(str(o), "{}")

    def test_clean(self):
        o = lo.Object()
        self.assertTrue(not o)

    def test_cleanload(self):
        o = lo.Object()
        o.test = "bla"
        p = o.save()
        o.load(p)
        self.assertEqual(type(o), lo.Object)

    def test_settingattribute(self):
        o = lo.Object()
        o.bla = "mekker"
        self.assertEqual(o.bla, "mekker")

    def test_checkattribute(self):
        o = lo.Object()
        with self.failUnlessRaises(AttributeError):
            o.mekker

    def test_underscore(self):
        o = lo.Object()
        o._bla = "mekker"
        self.assertEqual(o._bla, "mekker")

    def test_update(self):
        o1 = lo.Object()
        o1._bla = "mekker"
        o2 = lo.Object()
        o2._bla = "blaet"
        o1.update(o2)
        self.assertEqual(o1._bla, "blaet")

    def test_iter(self):
        o1 = lo.Object()
        o1.bla1 = 1
        o1.bla2 = 2
        o1.bla3 = 3
        res = sorted(list(o1))
        self.assertEqual(res, ["bla1","bla2","bla3"])

    def test_json1(self):
        o = lo.Object()
        d = json.dumps(o, cls=lo.ObjectEncoder)
        self.assertEqual(d, "{}")
        
    def test_json2(self):
        o = lo.Object()
        o.test = "bla"
        d = json.dumps(o, cls=lo.ObjectEncoder)
        oo = json.loads(d, cls=lo.ObjectDecoder)
        self.assertEqual(oo.test, "bla")

    def test_jsonempty(self):
        o = json.loads("", cls=lo.ObjectDecoder)
        self.assertEqual(type(o), lo.Object)
        
    def test_jsonempty2(self):
        o = json.loads("{}", cls=lo.ObjectDecoder)
        self.assertEqual(type(o), lo.Object)
        
    def test_jsonattribute(self):
        o = lo.Object()
        o.test = "bla"
        dstr = json.dumps(o, cls=lo.ObjectEncoder)
        o = json.loads(dstr, cls=lo.ObjectDecoder)
        self.assertEqual(o.test, "bla")

    def test_stamp(self):
        o = lo.Object()
        o.test = lo.Object()
        o.test.test = lo.Object()
        lo.stamp(o)
        self.assertTrue(o.test.test.stamp, "lo.Object")

    def test_overload1(self):
        class O(object):
            def bla(self):
                print("yo!")
        o = O()
        o.bla = "mekker"
        self.assertTrue(o.bla, "mekker")                

    def test_overload2(self):
        class O(object):
            def bla(self):
                return "yo!"
        o = O()
        a = o.bla()
        self.assertTrue(a, "yo!")                

    def test_overload3(self):
        class O(object):
            def bla(self):
                return "yo!"
        o = O()
        setattr(o, "bla", "mekker")
        with self.assertRaises(TypeError):
            o.bla()
            
    def test_overload4(self):
        class O(lo.Object):
            def bla(self):
                return "yo!"
        o = O()
        a = o.bla()
        self.assertTrue(a, "yo!")                
